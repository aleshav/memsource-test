# Role: SSH Keys
The role manages user authorized_keys for a shared memsource user on managed hosts.

## Usage
~~~
$ ansible-playbook -i test/hosts -u memsource test-playbook.yml --tag ssh_keys
~~~

## Configuration
Main parts of the role's configuration are _managed_persons_ dictionary and _enabled_keys_ list.

### _managed_persons_ dictionary
By default stored in role's defaults/main.yml contains definition of all persons' keys (divided by new line). Each key in the dictionary represents a person and each value is a dictionary of person's attributes. Currently, only ssh_keys attribute is used, but the structure was chosen to allow extensibility.

Example representation of 3 keys:
~~~
managed_persons:
  ales:
    ssh_keys: |
      ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBAAE27OJNcnUgKIiUBxGKvAULnvnAzNXsHRZSkT7qslupqKiv/xulvRhYqfuPd58dvc2M+DFVH1XK9ME3GUPQyVy3gFTztaSuJYVDSQFOmi+5RUGPYfvWiqbVCsPxZOdnwP1Wh4tuH+9xF5FRGMlVngi3DgP+Wq/g/8xiu8J78V5NwEfkw== alesh-ecdsa
      ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIID09ssnrJZzSx4T6HN4xzfOMiv8O6qxmtwGD7Dnal/g alesh-trezor-t1
  michal:
    ssh_keys: |
      ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCj5VdVC9UyAgABfRzUj7UscRZ+RkKWlj66bqBIkMiYs2XUgi48qvhlq4077c39aCp6BGW8p28rx8a1r/mKxcXSOET5n9Ka6tHtqiRSoQb2WFmNliAi6J37MB/Khahm5G90Lv65vhtamwnHhodMtO+LbrVDQHatig1G7PYYPrQ41rgw9MjGfaPthPOzWYxfrKNwyv0RiE57VdD+k7HVnX32tJg1fHKvkQvzX8Wk1Ho5LmHU003mPZF1z4MJ8AO7pxP2S5eSNW2ildhH6ZoQiPPOnH6LOqfp4j5S+lKFMgsQqUDvSFEKV65KYJUm9lOxv74bjnHMsLw662XVA0XZh7tF michal.kebrt@nb-michal
      ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDAzr9xQ/4Y7ZoS8vDn/k+vaY/jTfeIhPzCQES6fuWUJwnMWplqow+Gjku82kDr/Tgt3zhS5IZXekZy7t56AxVcTKsbQkIZ34ckErXpPvKRj/6A3A/6keAyBxPY4NwuA1QA6j32Qaurp0KMOL833UihMi80JTG5dYEszBP5nUq3dZN3+UU+uM3o8LW+48V8/S0XaIkC0xDhDMowewqv3TaSNg4aCNVI696E1qJqDqdC+dXMW+iGcbywYmaC6DdBYYDJW3wWXmf42ueA+zQzJpI+YDGFw//BeG6jXryY0nJ0r3HLlJuOLDh/INoejoGMUsmXC4QPIzwjAbkk8Xfc/AN3 michal.kebrt@ws-michal
  martin:
    ssh_keys: |
      ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAIEAqrEQFa8Im1tydF7YIVbGPX3Vt96qLIUqa7lzN/bZLR6JlkPiZ0AswX29OaUMVWvZtp92ixsP7X1qi4E1d7btDnWdqTWSGrm/t4i+KX9HV6AXAOWFUSXpMukmDrGGCBKf+8k6p8k49tkDMBzgeOeEIwJ6oQKB6HsANXYGw+9LeoM=
~~~


### _enabled_keys_ list
Usually stored in the vars file of a particular host or group, contains a list of users to be enabled.

Example: to enable _ales_'s and _michal_'s key for all servers, store the following in the _test/group_vars/all/enabled_keys.yml_ file:
~~~
enabled_keys:
  - ales
  - michal
~~~

Example: to enable only _ales_' key for the _web_ group, store the following in the _test/group_vars/web/enabled_keys.yml_ file:
~~~
enabled_keys:
  - ales
~~~

_WARNING:_ Please note that this overrides the previous definition for all servers as specific group vars has higher precedence. See https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable for possible variable definition places and their respective precedence. It might be good to define special access dedicated groups for the purpose of access control.
