# Role: Iptables
The role manages iptables rules

## Usage
~~~
$ ansible-playbook -i test/hosts test-playbook.yml --tag iptables
~~~

## Configuration
Configuration is composed of lists with _iptables_output_rules__to_merge_ suffix.

### Sample configuration

Having 2 configuration files, one for all hosts, another for hosts in the _web_ group:

group_vars/all/output_iptables.yml
~~~
default_iptables_output_rules__to_merge:
- name: http
  port: 80
  destination_groups:
  - web
- name: https
  port: 443
- name: dns
  destination_hosts:
  - 8.8.8.8
  - 1.1.1.1
  - 4.4.4.4
  - 1.1.2.2
  port: 53
  protocol: udp
~~~

group_vars/web/output_iptables.yml
~~~
web_iptables_output_rules__to_merge:
- name: ssh
  port: 22
  destination_groups:
  - lb
~~~

Would result in the the following rules being generated:

- all servers can access https port on any host
- all servers can access http port on any host in in the web group
- all servers can access UDP DNS service on one of the 4 listed IPs
- servers in the web group can access ssh port on any server in lb group